// function "copies" parent object to child's prototype
var inheritsFrom = function (child, parent) {
    child.prototype = Object.create(parent.prototype);
};

var Fly = function();
Fly.prototype = new RandomWalker();