var length = 15;
var persons = [];

var greyValue;

function setup() {
    createCanvas(innerWidth, innerHeight);
    frameRate(60);
    
    greyValue = createSlider(0, 255, 100);
    greyValue.position(20,20);
    
    for (var i = 0; i < length; i++) {
        var size = random(5,10);
        persons.push(new Person(size, size));
    }   
}

function draw() { 
    background(greyValue.value());
    
    for (var i = 0; i < length; i++) {
        persons[i].randomWalkToMouse();
        persons[i].displayFly();
    }
    
    text("Grey Scale", 165, 35);
}

function Person(sizeX, sizeY) {
    this.x = random(innerWidth);
    this.y =  random(innerHeight);
    this.sizeX = floor(sizeX);
    this.sizeY = floor(sizeY);
    
    this.rightChance = 25;
    this.leftChance = 50;
    this.downChance = 75;
    this.upChance = 100;
    
    this.speed = random(3,5);
    
    /* 
        function moves the person (speed) steps to a "random" direction
        random is set according to chances
    */
    this.randomWalkToMouse = function() {
        var direction = random(100);
        this.chanceHandler();

        if (direction < this.rightChance) {
          this.x = this.x + this.speed;
        }
        else if (direction < this.leftChance) {
          this.x = this.x - this.speed;
        }
        else if (direction < this.downChance) {
          this.y = this.y + this.speed;
        }
        else {
          this.y = this.y - this.speed;
        }
    }

    /* function moves person (speed) steps to a truly random direction */
    this.randomWalk = function() {
        var direction = floor(random(4));
    
        switch(direction) {
          case 0: this.x = this.x + this.speed;
            break;
          case 1: this.x = this.x - this.speed;
            break;
          case 2: this.y = this.y + this.speed;
            break;
          case 3: this.y = this.y - this.speed;
            break;
        }
    };
  
  /* 
    function calculates all chances for a situation a person can come to or be into 
    and changes the chances accordingly
  */
    this.chanceHandler = function() {
        var difference = 50;

        // if person is already close to mouse, changing chances to normal
        if (abs(mouseX - this.x) < 10 && abs(mouseY - this.y) < 10) {
            this.changeChances(25,50,75,100);
        }

        // if person is far
        else {
          // increasing chance to go LEFT UP
            if (mouseX <= this.x && mouseY <= this.y) { 
                if (abs(abs(mouseX - this.x) - abs(mouseY - this.y)) > difference) {
                    // if more closer to x than y, needs to go UP only
                    if (abs(mouseX - this.x) < abs(mouseY - this.y)) {
                        this.changeChances(25,50,65,100); // only up
                    }
                    else {
                        this.changeChances(15,50,75,100); // only left
                    }
                }
                else {
                  this.changeChances(15,50,65,100); // both left and up
                }
            }
          // increasing chance to go LEFT DOWN
            else if (mouseX <= this.x && mouseY >= this.y) {
                if (abs(abs(mouseX - this.x) - abs(mouseY - this.y)) > difference) {
                    // if more closer to x than y, needs to go DOWN only
                    if (abs(mouseX - this.x) < abs(mouseY - this.y)) {
                        this.changeChances(25,50,85,100); // only up
                    }
                    else {
                        this.changeChances(15,50,75,100); // only left
                    }
                } 
                else {
                    this.changeChances(15,50,85,100); // both left and up
                }
            }
          // increasing chance to go RIGHT UP
            else if (mouseX >= this.x && mouseY <= this.y) {
                if (abs(abs(mouseX - this.x) - abs(mouseY - this.y)) > difference) {
                    // if more closer to x than y, needs to go UP only
                    if (abs(mouseX - this.x) < abs(mouseY - this.y)) {
                        this.changeChances(25,50,65,100); // only up
                    }
                    else {
                        this.changeChances(35,50,75,100); // only right 
                    }
                }
                else {
                    this.changeChances(35,50,65,100); // both right and up
                }
            }
          // increasing chance to go RIGHT DOWN
            else if (mouseX >= this.x && mouseY >= this.y) {  
                if (abs(abs(mouseX - this.x) - abs(mouseY - this.y)) > difference) {
                    // if more closer to x than y, needs to go DOWN only
                if (abs(mouseX - this.x) < abs(mouseY - this.y)) {
                    this.changeChances(25,50,85,100); // only down
                }
                else {
                    this.changeChances(35,50,75,100); // only right
                }
            }
            else {
                this.changeChances(35,50,85,100); // both right and up
            }
          }
        }
    };    
    
    /* help function for practicly changing the chances */
    this.changeChances = function(right, left, down, up) {
        this.rightChance = right;
        this.leftChance = left;
        this.downChance = down;
        this.upChance = up;
    };
  
    /* protoype display, drawing the person*/
    Person.prototype.displayFly = function() {
        ellipse(this.x, this.y, this.sizeX, this.sizeY);
    };
  
};