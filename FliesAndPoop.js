var length;
var Flies = [];
var border = 10;
var greyValue;

var mainFont;

var poopImage;

function setup() {
    createCanvas(innerWidth, innerHeight);
    frameRate(60);
    length = floor(random(1000,1200));
    
    poopImage = loadImage("images/poop.gif");
    
    mainFont = loadFont("../fonts/font2.ttf", fontRead);   
    
    greyValue = createSlider(0, 255, 160);
    greyValue.position(20,20);
    
    for (var i = 0; i < length; i++) { 
        //var fly = new Fly();
        Flies.push(new RandomWalker());
    }   
}

function draw() { 
    createCanvas(innerWidth, innerHeight);
    background(greyValue.value());
    
    // poop image follow mouse
    image(poopImage, mouseX - poopImage.width / 2, mouseY - poopImage.height / 2);
    
    // mouse is not on screen
    if (mouseX <= border || mouseX >= width - border || mouseY <= border || mouseY >= height - border) {
    	for (var i = 0; i < length; i++) {
       	    Flies[i].randomWalk();
            point(Flies[i].x, Flies[i].y);
        }
        
        // upper sign
        drawBorder();
        text("Random", innerWidth / 2 - 35, 13);
    }
    // mouse is on screen
    else {
        for (var i = 0; i < length; i++) {
            Flies[i].chancesRandomWalk();
            point(Flies[i].x, Flies[i].y);
        }
	
	// upper sign
	drawBorder();
	text("Chasing Poop", innerWidth / 2 - 50, 13);
    }
    
    text("Grey Scale", 165, 35);
}

/* if font was successfuly loaded, reads it */
function fontRead() {
    textFont(mainFont);
    textSize(14);
}

/* function draws outside border*/
function drawBorder() {
    smooth(10);
    
    rect(0,0,innerWidth, border); // up
    rect(0,0,border, innerHeight); // left
    rect(0,innerHeight - border, innerWidth, border); // down
    rect(innerWidth - border,0,border, innerHeight); // right
    rect(innerWidth / 2 - 60, 0, 98, 20, 7); // box
}